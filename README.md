# Design Rationale

This product is essentially a replacement for Microsoft Producer, a discontinued program for 
creating presentations.

I chose to use Django because of its ease of use, documentation, ORM, and development speed. I've 
found that other frameworks like Flask or Node+Express offer too little to deliver a product 
quickly.

I've integrated this product with AWS. It is designed to run on an Amazon EC2 instance and use S3 
buckets for static files and uploads. I am using FakeS3 for development.

A presentation is comprised of a video and a powerpoint.

I've chosen to use Vimeo for video hosting because of the "plus" plan it offers. In addition to 
unlimited bandwith and a nice video player API, Vimeo can restrict viewing to certain URLs (useful 
for paywalled content). However, vimeo can be swapped out with something else if necessary.

This project does not come with a powerpoint editor. Instead, users can upload existing .pptx files 
and synchronize them with the video. Internally, powerpoint slides are converted to JPEG images 
using LibreOffice, unoconv, and ImageMagick. I'm using Celery (an asynchronous task queue) to handle 
the conversion. This process ignores any transitions and animations present in the powerpoint.

Embedding a presentation in a website is as simple as adding an iframe.

Features that could be added in the future:
- Recording video and/or audio from computer to add to presentation
- Video transcripts (automatic?)
