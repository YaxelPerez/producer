import factory

class VideoFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = 'core.Video'

    vimeo_id = 0xDEADBEEF

class SlideFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = 'core.Slide'

    slideshow = factory.SubFactory('core.factories.SlideshowFactory')
    image = factory.django.ImageField(width=1920, height=1440, color='green')
    start_time = factory.Sequence(lambda n: float(n))
    end_time = factory.Sequence(lambda n: float(n+1))

class SlideshowFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = 'core.Slideshow'

class PresentationFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = 'core.Presentation'

    name = factory.Sequence(lambda n: 'Presentation {}'.format(n))
    video = factory.SubFactory('core.factories.VideoFactory')
    slideshow = factory.SubFactory('core.factories.SlideshowFactory')
