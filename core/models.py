from django.db import models
from django.utils import timezone
from django.core.validators import MinValueValidator
from app.storage_backends import PrivateMediaStorage

class TimestampedModel(models.Model):
    """Model that automatically sets timestamps when created and edited"""
    created_on = models.DateTimeField()
    modified_on = models.DateTimeField()

    def save(self, *args, **kwargs):
        if not self.id:
            self.created_on = timezone.now()
        self.modified_on = timezone.now()
        return super().save(*args, **kwargs)

    class Meta:
        abstract = True

class Presentation(TimestampedModel):
    """Combination of video and PowerPoint presentation"""
    name = models.CharField(max_length=50, default='Untitled Presentation',)
    description = models.TextField(max_length=500, blank=True, null=True,)
    active = models.BooleanField(default=True)
    video = models.OneToOneField('core.Video', on_delete=models.CASCADE,)
    slideshow = models.OneToOneField('core.Slideshow', on_delete=models.CASCADE,)

class Slideshow(TimestampedModel):
    # just an empty model. More fields might be added in the future so I'm keeping it.
    pass

class Slide(models.Model):
    """A slide in a slideshow"""
    slideshow = models.ForeignKey('core.Slideshow', on_delete=models.CASCADE,)
    image = models.ImageField(storage=PrivateMediaStorage())
    start_time = models.FloatField(validators=[MinValueValidator(0.0),])
    end_time = models.FloatField(validators=[MinValueValidator(0.0),])

class Video(TimestampedModel):
    """A video accompanying a presentation"""
    vimeo_id = models.PositiveIntegerField()

