from django.urls import path

from core import views

urlpatterns = [
    path('test_upload/', views.TestUploadView.as_view(), name='test-upload-view'),
]
