from django.test import TestCase
from core.models import (
    TimestampedModel,
    Playlist,
    PlaylistItem,
    Presentation,
    Slideshow,
    Slide,
    Video,
)

from core.factories import (
    PresentationFactory,
)
