#  from tempfile import NamedTemporaryFile # .name
from app.celery import app
from celery.utils.log import get_task_logger
import subprocess
import uuid
import os
from pathlib import Path

logger = get_task_logger(__name__)

@app.task
def process_uploaded_powerpoint(file_path):
    logger.info('Processing {}'.format(file_path))
    pdf_path = '/tmp/' + uuid.uuid4().hex[:8] + '.pdf'
    logger.info('Running "unoconv -f pdf -o {} {}'.format(pdf_path, file_path))
    subprocess.run(('unoconv', '-f', 'pdf', '-o', pdf_path, file_path))

    out_name = uuid.uuid4().hex[:8]
    logger.info('Running "convert -density 400 {} -resize 1920x1440 {}_%d.jpg'.format(pdf_path, '/tmp/' + out_name))
    subprocess.run(('convert', '-density', '400', pdf_path, '-resize', '1920x1440', '/tmp/' + out_name + '_%d.jpg'))
    os.remove(pdf_path)

    p = Path('/tmp/')
    logger.info(list(p.glob('{}_[0-9]*.jpg'.format(out_name))))

    # create slide instances and save them

    return 0


