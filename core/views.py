from django.shortcuts import render
from django.views.generic.edit import FormView
from django.http import HttpResponse

from core import forms
from core import tasks

import os
import uuid

class TestUploadView(FormView):
    template_name = 'test_upload.html'
    form_class = forms.TestUploadForm

    def form_valid(self, form, **kwargs):
        if form.is_valid():
            # save file to /tmp. Task will clean it up
            upload = self.request.FILES.get('f')
            temp_file_path = '/tmp/' + uuid.uuid4().hex[:8] + os.path.splitext(upload.name)[1]
            with open(temp_file_path, 'wb+') as temp:
                for chunk in upload.chunks():
                    temp.write(chunk)
            tasks.process_uploaded_powerpoint.delay(temp_file_path)

        return HttpResponse('hey')
