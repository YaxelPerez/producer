# Thanks stackoverflow
# https://stackoverflow.com/questions/43919166/django-and-celery-re-loading-code-into-celery-after-a-change

import sys

import shlex
import subprocess
from django.core.management.base import BaseCommand
from django.utils import autoreload

class Command(BaseCommand):
    def handle(self, *args, **kwargs):
        autoreload.main(self._restart_celery)

    @classmethod
    def _restart_celery(cls):
        if sys.platform == 'win32':
            raise Exception('Win32 not supported.')
        cls.run("pkill -9 -f 'celery worker'")
        cls.run('celery worker -l info -A app')
    
    @staticmethod
    def run(cmd):
        subprocess.call(shlex.split(cmd))
